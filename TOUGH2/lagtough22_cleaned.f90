program lagtough
!*******************************************************************
!Version 22, 15 Juli 2014
!Manuel Lorenzo Sentis
!This program converts the mesh of LaGriT into a mesh for TOUGH2
!Be carefull and cross check the mesh created the subroutine can contain bugs!!!!
!Check for example if the number of vertices is consistent with the output of LaGriT
!
!It was tested for the creation of the mesh of the second exercise
!of the FORGE Project in 2013. The results were coherent with the results
!of other groups.
!A Paper about these results "Combination of LAGRIT and TOUGH2 for 3D
!two-phase flow simulations within the FORGE project" M.Sentis, C. Gable
!will be published soon.
!********************************************************************************
!********************************************************************************
!The program needs following input:
!4 files of the output of LaGriT:
!1 coord_file.fehmn
!2 coord_file.stor
!3 coord_file2.stor
!4 mate_material.zone
!Change the names of the materials according to your problem in label 19 below 
!and then compile the program again.
!******************************************************************************* 
!*******************************************************************************
!
!
!
!
!FORMATS FOR LAGRIT
!read the volumen of the elements
9000 format(5(1pe20.12))
!read number of nodes and number of connections
9010 format(5i10)
9050 format(i10,1pe20.12,1pe20.12,1pe20.12)
9060 format(1pe20.12,1pe20.12,1pe20.12)
9070 format(a1)
9075 format(a52)
9080 format(a10)
9090 format(a3)
9100 format(a5)
9110 format(a80)
9120 format(A5,10X,A5,2E11.6,10X,3E11.6)
9130 format(A10,15X,i5,3E11.6,1E10.4,10X)
9140 format(i10,9i11)
9150 format(A4)
9160 format(10X,I11)
9170 format(I10,2X,I10)
9180 format(I10,2X,A5)
!
!lagrit
!neq: number of elements
!iwtotl: number of connections
!ncont=neq+itotl+1
  real AHTX
  integer iwtotl,neq,ncont,num_area_coef,num_conn_max,aaa,MM
  integer dim,dim1,dim2,dim3
  integer q,r,i,j,k,l,m,n
!a counts the number of elements
!MM is the matrix of the materials the first index is the number of materials
!the second index is the number of nodes in one material
  dimension aaa(1000000), MM(10,1000000)
  integer iwtotl2,neq2,ncont2,num_area_coef2,num_conn_max2
!
  real volic,x,y,z,XX,YY,ZZ
  integer count,Acount
!volic is the volumen of the nodes
!x,y,z are the coordinates of the nodes
  dimension volic(1000000),x(1000000),y(1000000),z(1000000)
  dimension XX(20000000),YY(20000000),ZZ(20000000)
!count count the number of connections to one node
!Acount is the accumulativ number of connections, for example:
! the first node has 3 connections the second 5 the third 7 and
!the fourth 2 then count(1)=3, count(2)=5, count(3)=7, count(4)=2
!Acount(1)=3,Acount(2)=8,Acount(3)=15 and Acount(4)=17
  dimension count(0:1000000),Acount(0:1000000)
  real volic2
  dimension volic2(1000000)
!AAd are the areas of the connections divided by the distances between the nodes
!In tough2 the distances to the common area is required, therefore the factor 2
!AA are the areas between neighboring nodes
!Dist distances between neighboring nodes
!cosinus is required by tough2 and it is the cosinus of the angle between the line
!connecting the neighboring nodes and the vertical direction of the gravity vector
  real AAd,AA,Dist,cosinus,SQ
! real Area
  dimension AAd(20000000),AA(20000000),Dist(20000000)
  dimension cosinus(20000000),SQ(20000000)
! dimension Area(100,100)
! itemp dessigns the integers after the volume values, it comprises the neq+1 
!values giving the number of non vanishing matrix coefficients in each row
! and the iwtotl values giving the non vanishing components of the matrix 
  integer itemp,mmtemp,nntemp,NN
  dimension itemp(20000000),mmtemp(20000000),nntemp(1000000)
  integer itemp2,mmtemp2,nntemp2,countmat,nmat
  dimension itemp2(20000000),mmtemp2(20000000),nntemp2(1000000)
!material number of each node, loot at label 17
  dimension nmat(1000000)
!
!for converting from integers to ascii characters
  character*1 cc 
  character*10 numbers
  character*52 ch
  character*3 ttt
  character*5 ELE,MAT  
  character*4 nnum
!
  dimension ELE(1000000),MAT(1000000)
!
  Data numbers /'0123456789'/
!total of 41+41+12 = 94 characters to generate the name of the elements
!That is only needed for a very big mesh
!
!  Data ch /'@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz0123
! X456789!"#$%&()*+,-./:;<=>?{|}~'/
!  Data ch /'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012//
! X'3456789'/
!   ch='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv'
!   ch(59:62)='wxyz'
!Hier the number of characters is 52
  Data ch /'ABCDEFGHIJKLMNOPQRSTUVWXYZ#$%&+-/0123456789:;<=>?@^_'/
!
!LaGriT
!
!  character*5 name
!  real W1,W2,W3,W4,W5,W6,V1,V2,V3,U1
!  real M1,M2,M3,M4,M5,M6,D1

! File definitions
! INPUT
! The following files are necessary for the conversion. They are created by lagrit and have to be
! in the same folder as the conversion subroutine
! file with the coordinates of all the nodes
! open(3,file='./coord_file.fehmn',
! open(3,file='/scratch/tundra/zqu/data/small_scale/structured_triplane.fehmn',status='old')      
  open(3,file='/scratch/tundra/zqu/data/small_scale/unstructured_test.fehmn',status='old')      
!file with the areas/distances of all connections
  open(1,file='STOR_COEF/full_mesh.stor',status='old')
!file with the distances of all connections
  open(2,file='STOR_AREA/full_mesh2.stor',status='old')
!file with all the materials of all nodes
!  open(4,file='/scratch/tundra/zqu/data/small_scale/structured_triplane_material.zone',status='old')
  open(4,file='/scratch/tundra/zqu/data/small_scale/unstructured_test_material.zone',status='old')
!open(3,file='F:\tough\test12\input3',status='old')
!open(4,file='F:\tough\test12\input',status='old')      
!OUTPUT
!The converted mesh is in the file toughgeo.inp The other output files are check files. They could be deleted
!after the conversion
  open(11,file='./test.inp',status='unknown')
  open(12,file='./toughgeo.inp',status='unknown')
  open(14,file='./test2.inp',status='unknown')
  open(15,file='./test3.inp',status='unknown')
  open(16,file='./SmallVolumenElements',status='unknown')

!******************
! PART OF THE INPUT BEFORE THE GEOMETRY
!******************
!initialization
  AHTX=0.0000E+00
  dim=1000000
  dim2=20000000

  do i=1,dim
    volic(i)=0.0
    volic2(i)=0.0
    x(i)=0.0
    y(i)=0.0
    z(i)=0.0
    count(i)=0
    Acount(i)=0
    nntemp(i)=0
    nntemp2(i)=0
    MAT(i)='     '
    nmat(i)=0
    aaa(i)=0
  enddo
  do j=1,dim2
    AAd(j)=0.0
    AA(j)=0.0
    Dist(j)=0.0
    cosinus(j)=0.0
    SQ(j)=0.0
    XX(j)=0
    YY(j)=0
    ZZ(j)=0
    itemp(j)=0
    itemp2(j)=0
    mmtemp(j)=0
    mmtemp2(j)=0
  enddo
  do l=1,10
    do m=1,1000000
      MM(l,m)=0
    enddo
  enddo
  
!read in file coord_file.stor
  read(1,*)
  read(1,*)  
  read(1,9010) iwtotl,neq,ncont,num_area_coef,num_conn_max 
  read(1,9000) (volic(i),i=1,neq)
  read(1,9010) (itemp(i),i=1,ncont)
!count the number of elements which are connected with an element 
!
  do j=1,neq
    count(j)=itemp(j+1)-itemp(j)
  enddo
  read(1,9010) (mmtemp(i),i=1,ncont)
  read(1,9010) (nntemp(i),i=1,neq)
!read the areas divided by the distances
  read(1,9000) (AAd(i),i=1,iwtotl)
!read the coordinates of the nodes in the file coord_file.fehmn
  read(3,*)
  read(3,*)
  read(3,9050) (aaa(i),x(i),y(i),z(i),i=1,neq)
  read(2,*)
  read(2,*)  
  read(2,9010) iwtotl2,neq2,ncont2,num_area_coef2,num_conn_max2 
!the aim is here to obtain the areas between elements the other variables
!here with the number 2 are not used in the program
  read(2,9000) (volic2(i),i=1,neq2)
  read(2,9010) (itemp2(i),i=1,ncont2)
  read(2,9010) (mmtemp2(i),i=1,ncont2)
  read(2,9010) (nntemp2(i),i=1,neq2)
!read the areas
  read(2,9000) (AA(i),i=1,iwtotl2)
!calculate the distances between elements
!
  do 5 j=1,iwtotl
    if ( AAd(j).eq.0.0 ) then
      goto 5
    end if
    Dist(j)=AA(j)/(2*AAd(j))
5 continue
!calculate the cosine of the angle between elements and vertical direction
   do 10 n=0,127
    cc=char(n)    
    write(11,9070) cc 
10 continue
  write(11,9070) numbers(3:3)
  write(11,9070) ch(45:45)
  write(11,9070) ch(46:46)
  write(11,9070) ch(47:47) 
  write(11,9080) ch(40:50) 
  ttt=   numbers(3:3)//ch(45:45)// ch(46:46)   
  write(11,9090) ttt 
  write(11,9070) ch(1:1)
  write(15,*) "important" 
  write(15,9075) ch(1:52) 
  write(15,9070) ch(1:1)    
!group the elements with the same materials
  countmat=0
  do 15 i=1,100000 
    read(4,9150) nnum
    if (nnum .eq. 'nnum') then
      goto 18          
    endif
    if (nnum .eq. 'stop') then
      write (*,9150) nnum
      goto 17          
    endif
  goto 15
18 continue
!read the elements belonging to the given material
  countmat=countmat+1
  read(4,9160) NN
  read(4,9140) (MM(countmat,q),q=1,NN)
  write(11,9150) nnum
  
!  MATERIALS:
! This materials are user specific and have to be changed according to the model
!1 Clay, clayr
!(2) waste, waste
!3 Bentonite in cavern, bento
!(4) Interface facing waste(no used here)
!5 EDZ cavern, access drift, main drift, edzal
!6 Backfill access drift,main drift, backf
!(7) Interface facing bentonite(no used here)
!(8) Interface facing backfill and edz(no used here)
!(9) boundary elements with big volumes (no used here)
!(10) boundary elements with special boundary conditions (no used here)
!11 source elements (2 discs per cavern),sourc
!12 Modified bentonite in main drift, mbent
!13 edz in front of waste, medzw (interface)
!14 edz main drift long, mlbkf
!15 edz main drift short, msbkf
!(16) edz before the tunnel, edzbt (==> 5), converted in 5!!!
!17 edz before the cavern, mbcez
!(18) edz after the tunnel, edzat
!19 edz in front of bentonite, mbeca (interface)
!     20 EDZ access drift modified (interface), modified backfill acess drift, mbkad
!
!
!***************************
!This materials are user specific and the names have to be changed according
!to the model.
!According to the TOUGH2 specifications the name of the material
!is a string of 5 characters.
!**************************************************** 
19 continue
  do r=1,NN
    nmat(MM(countmat,r))=countmat
    if (countmat .eq. 1) MAT(MM(countmat,r))='clayr'
    if (countmat .eq. 2) MAT(MM(countmat,r))='waste'
    if (countmat .eq. 3) MAT(MM(countmat,r))='bento'
    if (countmat .eq. 4) MAT(MM(countmat,r))='inwas'
    if (countmat .eq. 5) MAT(MM(countmat,r))='edzal'
    if (countmat .eq. 6) MAT(MM(countmat,r))='backf'
    if (countmat .eq. 7) MAT(MM(countmat,r))='inben'
    if (countmat .eq. 8) MAT(MM(countmat,r))='inbac'
    if (countmat .eq. 9) MAT(MM(countmat,r))='boun1'
    if (countmat .eq. 10) MAT(MM(countmat,r))='bounf'
    if (countmat .eq. 11) MAT(MM(countmat,r))='sourc'
    if (countmat .eq. 12) MAT(MM(countmat,r))='mbent'
    if (countmat .eq. 13) MAT(MM(countmat,r))='medzw'
    if (countmat .eq. 14) MAT(MM(countmat,r))='mlbkf'
    if (countmat .eq. 15) MAT(MM(countmat,r))='msbkf'
    if (countmat .eq. 16) MAT(MM(countmat,r))='edzal'
    if (countmat .eq. 17) MAT(MM(countmat,r))='mbcez'
    if (countmat .eq. 18) MAT(MM(countmat,r))='edzat'
    if (countmat .eq. 19) MAT(MM(countmat,r))='mbeca'
    if (countmat .eq. 20) MAT(MM(countmat,r))='mbkad'
    write(14,9170) MM(countmat,r),nmat(MM(countmat,r))
  enddo
  write(11,9140) (MM(countmat,q),q=1,NN)
  if (nmat(neq) .ne. 0) write(*,*) "assigned Materials"
15  continue 
17  continue
!
!read the elements and write them in the format of tough2
!WRITE ELEMENTS TO Tough2
  write(12,*)
  write(12,9110)'ELEME----1----*----2----*----3----*----4--'//&
    '--*----5----*----6----*----7----*----8' 
  n=0
  do 20 i=1,52
    do j=1,52
      do k=1,52
        do l=1,10
          do m=1,10
            n=n+1
            if ( n .gt. neq ) then
              goto 30
            end if
            ELE(n)=ch(i:i)//ch(j:j)//ch(k:k)//numbers(l:l)//&
                      numbers(m:m)
! mls 22.3.2013 to increase very small volumes (<0.9e-3) because they &
! give convergence problems
!
!                     if (volic(n) .le. 0.9e-3) then
!                        write(16,9120) ELE(n),MAT(n),volic(n),AHTX,x(n),
!     X                              y(n),z(n)                       
!                        volic(n)=0.9e-2
!                     endif
!mls 22.3.2013
            write(12,9120) ELE(n),MAT(n),volic(n),AHTX,x(n),&
                                  y(n),z(n)
          enddo
        enddo
      enddo
    enddo  
20 continue
!
30 continue
!*
!*
!Write Connections to Tough2
! WRITE CONNECTIONS
  write(12,*)
  write(12,9110)'CONNE----1----*----2----*----3----*----4--'//&
     '--*----5----*----6----*----7----*----8' 
!
  count(0)=0
  Acount(0)=0       
  do j=1,neq
    Acount(j)=Acount(j-1)+count(j)
    do 40 i=1,count(j)
!calculate the cosinus of angle
!
      if (j .ge. itemp(neq+1+i+Acount(j-1))) then
        goto 40
      end if
      XX(Acount(j-1)+i)=(x(j)-x(itemp(neq+1+i+Acount(j-1))))**2
      YY(Acount(j-1)+i)=(y(j)-y(itemp(neq+1+i+Acount(j-1))))**2
      ZZ(Acount(j-1)+i)=(z(j)-z(itemp(neq+1+i+Acount(j-1))))**2
      SQ(Acount(j-1)+i)=sqrt(XX(Acount(j-1)+i)+YY(Acount(j-1)+i)+&
      ZZ(Acount(j-1)+i))
      cosinus(Acount(j-1)+i)=z(j)-z(itemp(neq+1+i+Acount(j-1))) &
              /SQ(Acount(j-1)+i)
!MLS IMPORTANT
!mls 20.3.2013 Eliminate the connections with 0 Area, they result in 
!NaNQ or INF numbers is the output 
!mls 20.3.2013 Dont write connections with 0 distances or 0 areas and
! dont write connections with small areas
      if ( Dist(Acount(j-1)+i) .eq. 0.0) goto 40
      if ( AA(Acount(j-1)+i) .eq. 0.0) goto 40
      if ( ABS(AA(Acount(j-1)+i)) .lt. 1.0e-12) goto 40
      write(12,9130) ELE(j)&
           //ELE(itemp(neq+1+i+Acount(j-1))),1,Dist(Acount(j-1)+i),&
           Dist(Acount(j-1)+i),ABS(AA(Acount(j-1)+i)),&
           cosinus(Acount(j-1)+i)
40 continue             
  enddo
  close (3)       
  close (1) 
  close (2)  
  close (11) 
  close (4)
  close (12)
  close (14)
  close (15)
end program lagtough
