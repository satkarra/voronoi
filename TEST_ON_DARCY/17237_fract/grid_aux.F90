module Grid_Aux_module

  implicit none

  private 
  
#include "petsc/finclude/petscsys.h"
#include "petsc/finclude/petscvec.h"
#include "petsc/finclude/petscvec.h90"
#include "petsc/finclude/petscis.h"
#include "petsc/finclude/petscis.h90"
#include "petsc/finclude/petscmat.h"
#include "petsc/finclude/petscmat.h90"
  
  type, public :: point3d_type
    PetscReal :: x
    PetscReal :: y
    PetscReal :: z
  end type point3d_type
  
  type, public :: grid_type
    PetscInt, pointer :: vertex_ids(:)
    PetscInt, pointer :: vertex_ids_need(:)
    PetscInt, pointer :: vertex_ids_map(:)
    PetscInt, pointer :: elem_ids(:)
    PetscInt, pointer :: elem_connectivity(:,:)
    PetscInt :: num_elems_global  ! Number of cells in the entire domain
    PetscInt :: num_elems_local   ! Number of elements locally
    PetscInt :: num_elems_max     ! Max of all the number of elements locally
    PetscInt :: num_pts_local
    PetscInt :: num_pts_need
    PetscInt :: num_pts_global
    Vec :: coordinates_local ! (series vector)    local stored coordinates depending
                             !                    on the connectivity on current processor
    Vec :: coordinates_vec   ! (parallel vector)  store all the coordinates 
                             !                    in a 3*num_pts long vector
    Vec :: connections       ! (parallel vector)  rough estimate of the connections 
    Vec :: degree            ! (upper triangular/parallel vector) 
                             !                    true degree of connections for vertices
    Vec :: degree_tot        ! (full matrix/parallel vector)
                             !                    exact degree of connections for vertices 
    Mat :: adjmatrix         ! (upper triangular) sparse parallel matrix: ajacent matrix 
                             !                    that contains the geometric coefficients
    Mat :: adjmatrix_full    ! (full matrix/parallel matrix) ajacent matrix
                             !                    that contains the geometric coefficients
    Mat :: edgematrix        ! (full matrix/parallel matrix) records the edge list
  end type grid_type

  public :: GridCreate, &
            GridDestroy

contains

! ************************************************************************** !

function GridCreate()
  ! 
  ! Creates an grid object
  ! 
  ! Author: Satish Karra & Zhuolin Qu, LANL
  ! Date: 07/21/2015
  ! 

  implicit none
  
  type(grid_type), pointer :: GridCreate
  type(grid_type), pointer :: grid

  allocate(grid)

  nullify(grid%vertex_ids)
  nullify(grid%vertex_ids_need)
  nullify(grid%vertex_ids_map)
  nullify(grid%elem_ids)
  nullify(grid%elem_connectivity)
  grid%num_elems_global = 0
  grid%num_elems_local = 0
  grid%num_elems_max = 0
  grid%connections = 0
  grid%degree = 0
  grid%degree_tot = 0
  grid%adjmatrix = 0
  grid%edgematrix = 0

  GridCreate => grid
  
end function GridCreate

! ************************************************************************** !

subroutine GridDestroy(grid)
  ! 
  ! Destroys an grid object
  ! 
  ! Author: Satish Karra & Zhuolin Qu, LANL
  ! Date: 07/21/2015
  ! 

  implicit none
  
  type(grid_type), pointer :: grid
  PetscErrorCode :: ierr

  if (grid%connections /= 0) then
    call VecDestroy(grid%connections,ierr);CHKERRQ(ierr)
  endif
  if (grid%adjmatrix /= 0) then
    call MatDestroy(grid%adjmatrix,ierr);CHKERRQ(ierr)
  endif
  if (grid%adjmatrix_full /= 0) then
    call MatDestroy(grid%adjmatrix_full,ierr);CHKERRQ(ierr)
  endif
  if (grid%edgematrix /= 0) then
    call MatDestroy(grid%edgematrix,ierr);CHKERRQ(ierr)
  endif
  if (grid%coordinates_local /= 0) then
    call VecDestroy(grid%coordinates_local,ierr);CHKERRQ(ierr)
  endif
  if (grid%coordinates_vec /=0) then
    call VecDestroy(grid%coordinates_vec,ierr);CHKERRQ(ierr)
  endif
  if (grid%degree /= 0) then
    call VecDestroy(grid%degree,ierr);CHKERRQ(ierr)
  endif
  if (grid%degree_tot /= 0) then
    call VecDestroy(grid%degree_tot,ierr);CHKERRQ(ierr)
  endif 

  if (.not.associated(grid)) return
  deallocate(grid%vertex_ids)
  deallocate(grid%elem_ids)

end subroutine GridDestroy

end module Grid_Aux_module
