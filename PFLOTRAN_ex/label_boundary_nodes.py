from sys import *
import os

def redefine_zones(lagrit_path, h):
	lagrit_input = '''
read / gmv / full_mesh.gmv / mo 
cmo / printatt / mo/ -xyz- / minmax 
finish
	'''
	f=open('domainattr.lgi','w')
	f.write(lagrit_input) 
	f.flush()
	f.close()
	os.system(lagrit_path + "  < domainattr.lgi > printxyz.out")
	# python script to read data from lagrit output file 
	fil = open('printxyz.out','r')
	for line in fil:
		k=line.split()
		for word in line.split():
			if word == 'xic':
				x_min = float(k[1])
				x_max = float(k[2])
			if word=='yic':
				y_min = float(k[1])
				y_max = float(k[2])
			if word=='zic':
				z_min = float(k[1])
				z_max = float(k[2])

	fil.close()
	#lagrit scripts to create new zone files: boundary zones

	eps = h*0.001
	
	parameters = (x_max - eps, x_min + eps, y_max - eps, \
			 y_min + eps, z_max - eps, z_min + eps)
	lagrit_input = '''
read / gmv / full_mesh.gmv / mo
define / XMAX / %e 
define / XMIN / %e 
define / YMAX / %e 
define / YMIN / %e 
define / ZMAX / %e 
define / ZMIN / %e 

cmo / addatt / mo / top  
pset / ptop / attribute / zic / 1,0,0/ gt /ZMAX 
cmo / setatt / mo / top/ pset, get, ptop / 1


cmo / addatt / mo / bottom  
pset / pbottom / attribute / zic / 1,0,0/ lt /ZMIN 
cmo / setatt / mo / bottom / pset, get, ptop / 1


cmo / addatt / mo / left_w  
pset / pleft_w / attribute/ xic/ 1,0,0 /lt / XMIN
cmo / setatt / mo / left_w / pset, get, pleft_w / 1


cmo / addatt / mo / right_e 
pset / pright_e / attribute/ xic/1,0,0/ gt/XMAX
cmo / setatt / mo / right_e / pset, get, pright_e / 1

cmo / addatt / mo / front_s
pset / pfront_s / attribute/ yic / 1,0,0 / gt/YMAX
cmo / setatt / mo / front_s / pset, get, pfront_s / 1

cmo / addatt / mo / back_n 
pset / pback_n / attribute/ yic/ 1,0,0 / lt /YMIN
cmo / setatt / mo / back_n / pset, get, pback_n / 1

cmo / addatt / mo / boundary 
cmo / setatt / mo / boundary / pset, get, ptop / 1
cmo / setatt / mo / boundary / pset, get, pbottom / 1
cmo / setatt / mo / boundary / pset, get, pleft_w / 1
cmo / setatt / mo / boundary / pset, get, pright_e / 1
cmo / setatt / mo / boundary / pset, get, pfront_s / 1
cmo / setatt / mo / boundary / pset, get, pback_n / 1

dump / avs / full_mesh_tagged.inp / mo 
dump / gmv / full_mesh_tagged.gmv / mo 
finish

'''
	f=open('bound_zones.lgi','w')
	f.write(lagrit_input%parameters)
	f.flush()
	f.close()

	os.system(lagrit_path + "  < bound_zones.lgi ")

h = float(sys.argv[1])
lagrit_dfn = '/n/swdev/LAGRIT/bin/lagrit_lin'
redefine_zones(lagrit_dfn, h)



