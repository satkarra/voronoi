This code evaluates the voronoi areas and volumes and other information for given Delauney mesh.
There are a couple of data files in /data/ directory. Data files for more detailed test problems are in 
/scratch/tundra/zqu/data/DFN_network folder

1. To use VORONOI first install PETSc based on the following instructions.

    PETSc version used:
    
        git clone https://bitbucket.org/petsc/petsc petsc
        cd petsc
        git checkout xsdk-0.2.0
    
    PETSc configuration: Configure PETSc by first setting PETSC_DIR and PETSC_ARCH environmental variables and then using:
    
        ./configure --download-mpich=yes --download-metis=yes --download-parmetis=yes
        
    Compile PETSc using the instructions you see on the screen after configuring. You should see something similar to:
    
        xxx=========================================================================xxx
        Configure stage complete. Now build PETSc libraries with (gnumake build):
        make PETSC_DIR=/Users/satkarra/src/petsc-3.7-release PETSC_ARCH=macx-nodebug all
        xxx=========================================================================xxx
    
2. Compile VORONOI by going into the repository directory and then typing:
        
        make voronoi