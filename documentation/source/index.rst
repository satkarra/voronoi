.. VORONOI documentation master file, created by
   sphinx-quickstart on Thu Jun 22 13:22:55 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VORONOI's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   algorithms
   input_file
   running



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

